package com.zbkc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 *  @author gmding
 * */
@EnableSwagger2
@SpringBootApplication
@Configuration
@MapperScan("com.zbkc.dao")
public  class ZbkcDm8Application {
    public static void main(String[] args) {
        SpringApplication.run(ZbkcDm8Application.class, args);
    }

}
