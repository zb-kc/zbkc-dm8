package com.zbkc.service;

import com.zbkc.dao.imp.SysUserImp;
import com.zbkc.dao.SysUserDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
/**
 *  @author gmding
 *
 * */
@Service
public class SysUserService implements SysUserImp , Serializable {

    @Autowired
    SysUserImp sysUserImp;

    @Override
    public List<SysUserDo> queryByList() {
        return sysUserImp.queryByList();
    }

}
