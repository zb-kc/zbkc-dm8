package com.zbkc.dao.imp;

import com.zbkc.dao.SysUserDo;

import java.util.List;

/**
 *
 *  @author gmding
 * */
public interface SysUserImp {

    /**
     * 查询列表
     * @@return List<SysUserDao>
     * */
    List<SysUserDo> queryByList();

}
