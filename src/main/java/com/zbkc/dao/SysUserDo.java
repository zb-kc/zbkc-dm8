package com.zbkc.dao;


import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *  @author gmding
 * */
@Data
public class SysUserDo implements Serializable {

    int id;
    String userName;
    String nickName;
    String pwd;
    int sex;
    int age;
    int status;
    Timestamp createTime;
    String creater;
    Timestamp updateTime;
}
