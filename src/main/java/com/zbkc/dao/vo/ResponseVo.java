package com.zbkc.dao.vo;

import lombok.Data;

import java.io.Serializable;

/***
 *  @author gmding
 */

@Data
public class ResponseVo implements Serializable {
    /**
     *状态码
     */
    public int code;
    /**
     * 返回信息
     * */
    public String errMsg;
    /**
     * 返回数据
     * */
    public Object data;


    public ResponseVo(int code, String errMsg, Object data){
        this.code=code;
        this.errMsg=errMsg;
        this.data=data;
    }


    public ResponseVo(){
        super();
    }


}
