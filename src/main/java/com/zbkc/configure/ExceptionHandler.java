package com.zbkc.configure;

import com.google.gson.Gson;
import com.zbkc.dao.vo.ResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *  @author gmding
 * */

@ControllerAdvice
@Slf4j
public class ExceptionHandler {


    /**
     * 处理BusinessException异常
     * @param e
     * @return
     */
    @org.springframework.web.bind.annotation.ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ResponseVo busin(BusinessException e) {
        log.info("data:{}",new Gson().toJson(e));
        return  new ResponseVo(e.getCode(), e.getErrMsg(),null);
    }

}