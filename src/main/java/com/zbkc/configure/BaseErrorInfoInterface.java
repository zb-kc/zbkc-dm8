package com.zbkc.configure;

/**
 * @author gmding
 *
 * */

public interface BaseErrorInfoInterface {

    /**
     *  错误码
     * @return
     */
    int getStatus();

    /**
     * 错误描述
     * @return
     */
    String getMsg();
}
