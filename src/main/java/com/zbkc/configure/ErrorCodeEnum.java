package com.zbkc.configure;

/**
 * 状态码描述管理
 * @author gmding
 * */
public enum ErrorCodeEnum{

    //未授权认证
    UNAUTHORIZED(401,"未授权认证"),
    //未找到该资源
    RESOURCE_NOT_FOUND(1001,  "未找到该资源");

    private int code;

    private String errMsg;

    ErrorCodeEnum(int code,  String errMsg) {

        this.code = code;
        this.errMsg = errMsg;
    }

    public int getCode() {
        return code;
    }



    public String getErrMsg() {
        return errMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", errMsg='" + errMsg + '\'' +
                '}';
    }
}