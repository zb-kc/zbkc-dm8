package com.zbkc.configure;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 自定义拦截器
 * @author gmding
 */
@Slf4j
public class MyInterceptor implements HandlerInterceptor {

    public final SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'.'SSS");
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("startTime:{}",formatter.format(new Date(System.currentTimeMillis())));
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        HandlerMethod handlerMethod = null;
        
        try {
            handlerMethod= (HandlerMethod) handler;
        }catch (Exception e){
            return true;
        }
        
        Method method = handlerMethod.getMethod();
        String methodName = method.getName();

        String path=request.getContextPath()+request.getServletPath();


        log.info("path:{}",path);
        log.info("methodName:{}",methodName);

        for (String url:BasePathPatterns.URL){
            boolean flag=path.matches(url);
            if (flag){
                return true;
            }
        }
        String token = request.getParameter("token");
        if (null == token || "".equals(token)) {
            log.info("用户未登录，没有权限执行……请登录");
            throw  new BusinessException(ErrorCodeEnum.UNAUTHORIZED);
            //return false;
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //log.info("执行完方法之后进执行(Controller方法调用之后)，但是此时还没进行视图渲染");
        //log.info("方法执行完时间:{}",formatter.format(new Date(System.currentTimeMillis())));
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("endTime:{}",formatter.format(new Date(System.currentTimeMillis())));


    }



}
