package com.zbkc.configure;


/**
 *  @author gmding
 * */
public class BusinessException extends RuntimeException{


    private int code;

    private String errMsg;

    public  BusinessException(ErrorCodeEnum errorCode){
        this.code = errorCode.getCode();
        this.errMsg = errorCode.getErrMsg();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }


}
