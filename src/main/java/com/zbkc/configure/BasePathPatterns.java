package com.zbkc.configure;

/**
 * 过滤路径
 * @author gmding
 * */

public class BasePathPatterns {

    /**
     * 过滤拦截器路径,适合正则
     * @param string
     *
     * */
    public final static String[] URL = {
            "/api/v1/zbkc/text/login(.*)",
            "/api/v1/zbkc/text(.*)",
            "/api/v1/zbkc/swagger-resources(.*)",
            "/api/v1/zbkc/error"

    };
}
